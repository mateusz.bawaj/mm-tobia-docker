FROM dodasts/snj-base-lab:v1.0.0-snj

RUN wget https://repo.anaconda.com/miniconda/Miniconda3-py38_4.11.0-Linux-x86_64.sh \
  && bash Miniconda3-py38_4.11.0-Linux-x86_64.sh -b \
  && rm Miniconda3-py38_4.11.0-Linux-x86_64.sh
  
ENV PATH /root/miniconda3/bin:$PATH
  
COPY tobia-environment.yml /root

RUN ["/bin/bash", "-c", "/root/miniconda3/bin/activate \
  && conda install -y ipykernel \
  && ipython kernel install --name=conda \
  && conda env update -n base --file /root/tobia-environment.yml  --prune" ]
#  && conda deactivate

# docker run -it --entrypoint /bin/bash matib12/mm_tobia:1.0
