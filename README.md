# How to create a docker container for the INFN-cloud collaborative Jupyter notebook.

If you want to use it in the INFN-cloud you need to have a docker hub account.

1. change to the project directory
```bash
$ cd mm-tobia-docker
```

2. customize your environment by modifying the tobia-environment.yml
```bash
$ docker build -t [your_user]/mm_tobia:[version] .
```
3. send to the docker hub
```bash
$ docker push [your_user]/mm_tobia:[version]
```
